'''
Author: Jose Delgado
Purpose: Given a range of (MIN, MAX), determine if a number is in that range
'''

def ran_check(num,low,high):
    '''
    Check if "num" is between the range of low & high
    '''
    if num >= low and num <= high:
        return "In range"
    else:
        return "Not in range"

    
minR = raw_input("Enter the min for the range: ")
maxR = raw_input("Enter the max for the range: ")
numb = raw_input("Enter a number: ")

inRange = ran_check(int(numb),int(minR),int(maxR))
print "The number {n} is: {r}".format(n = numb, r = inRange)