def unique_list(l):
    '''
    Return a list of unique values in a given list
    '''
    mySet = set(l)
    return list(mySet)

myList = unique_list([1,1,1,1,2,2,3,3,3,3,4,5])

print "Original list: [1,1,1,1,2,2,3,3,3,3,4,5]"
print "Unique List: {l}".format(l = myList)
    