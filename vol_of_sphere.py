import math

def vol(rad):
    '''
    Compute the volume of a sphere, given it's radius
    Vol = (4/3)*Pi*r^3
    '''
    FOUR_THIRDS = 4.0 / 3
    PI = math.pi
    radCubed = rad**3
    
    return (FOUR_THIRDS * PI * radCubed)

myRad = raw_input("Enter the radius to calculate the volume of a sphere: ")
integerRad = int(myRad)

volOfSphere = vol(integerRad)
print "The volume of the sphere is: ", volOfSphere

