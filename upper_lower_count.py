def up_low(s):
    '''
    Count the number of upper & lower case characters in a string
    Return the counts
    '''
    d = {'upper':0, 'lower':0}
    for c in s:
        if c.isupper():
            d['upper'] += 1
        elif c.islower():
            d['lower'] += 1
        else:
            pass
    return d

st = 'Hello Mr. Rogers, how are you this fine Tuesday?'
counts = up_low(st)

print "No. of Upper case chars: %d" %counts['upper']
print "No. of Lower case chars: %d" %counts['lower']
