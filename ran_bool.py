'''
Author: Jose Delgado
Purpose: Check if a value is within a given range
'''

def ran_bool(num,low,high):
    '''
    Precondition: int number, range min, range max
    Postcondition: true is in range, false otherwise
    '''
    if num >= low and num <= high:
        return True
    else:
        return False


minRange = raw_input("Enter the minimum of the range: ")
maxRange = raw_input("Enter the maximum of the range: ")
numb = raw_input("Enter the number to check: ")

inRange = ran_bool(int(numb), int(minRange), int(maxRange))

print "The number {n} is in range? {a}".format(n = numb, a = inRange)
