'''
Author: Jose Delgado
Purpose: Multiply all the numbers in a list & return the result
'''

def multiply(numbers):
    '''
    Input: list
    Output: product of the list
    '''
    product = 1
    for num in numbers:
        product *= num
        
    return product

l = [1,2,3,-4]
theProduct = multiply(l)

print "The product of {lst} is {p}".format(lst = l, p = theProduct)
    