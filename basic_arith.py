''' 
Author: Jose Delgado
Date: 11/24/15
Purpose: Basic arithmetic in Python
'''

#Addition
print "Integer Addition: ", 2 + 1

#Subtraction
print "Integer Subtraction: ", 2 - 1

#Multiplication
print "Integer Multiplication: ", 2 * 2

#Integer Division
print "Integer Division: ", 3 / 2

#Float Division
print "Float Division: ", 3.0 / 2

#Different precision float division
print "More Precision Division: ", 3.0000 + 3.1234

#Powers
print "Powers: ", 2**2

#Square root
print "Square Root: ", 4**0.5