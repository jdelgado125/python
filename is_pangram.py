'''
Author: Jose Delgado
'''

import string

def ispangram(str1, alphabet=string.ascii_lowercase):  
    str1 = ''.join(char for char in str1 if char.isalpha())
    str1 = str1.lower()
    setString = set(str1)
    setAlpha = set(alphabet)
    
    return setString==setAlpha
    
pangram = ispangram("The quick brown fox jumps over the lazy dog")
print pangram