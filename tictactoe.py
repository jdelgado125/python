def printTicTacToe(theBoard):
    '''
    Tic Tac Toe board being printed
    '''
    
    print '''
    {o} | {t} | {T}
    __|___|__
    {f} | {F} | {s}
    __|___|__
    {S} | {e} | {n}
      |   |
    '''.format(o = theBoard[0],
               t = theBoard[1], 
               T = theBoard[2], 
               f = theBoard[3], 
               F = theBoard[4], 
               s = theBoard[5], 
               S = theBoard[6], 
               e = theBoard[7], 
               n = theBoard[8])

def printEmptyBoard():
    print '''
    
    __|__|__
    __|__|__
      |  |
      
      '''

def validateUserInput(choice):
    if not (choice == 'X' or choice == 'O'):
        return False
    else:
        return True

def getPlayerList(token):
    if token == 'X':
        return {"p1":'X', "p2":'O'}
    else:
        return {"p1":'O', "p2":'X'}
    
def didPlayerWin(board, player):
    '''Check all possible win scenarios after play has made his turn'''
    if (board[0] == board[1] == board[2] == player) or \
       (board[3] == board[4] == board[5] == player) or \
       (board[6] == board[7] == board[8] == player) or \
       (board[0] == board[3] == board[6] == player) or \
       (board[1] == board[4] == board[7] == player) or \
       (board[2] == board[5] == board[8] == player) or \
       (board[0] == board[4] == board[8] == player) or \
       (board[2] == board[4] == board[6] == player):
        return True
    else:
        return False
    
def isSpotTaken(gameBoard, spotNum):
    if gameBoard[spotNum] == ' ' or gameBoard[spotNum] == '':
        return False
    else:
        print "Spot {n} is already taken!".format(n = spotNum + 1)
        return True
    
def isBoardFull(gameBoard):
    #for space in gameBoard:
        #if space == ' ':
            #return False
    #Went through whole board & no space is empty    
    #return True
    if ' ' in gameBoard[0:]:
        return False
    else:
        return True

def makeTheMove(gameBoard, move, player):
    gameBoard[move] = player
    
def getPlayersMove(board, chosenMove, player):
    while chosenMove not in '1 2 3 4 5 6 7 8 9'.split() or \
          isSpotTaken(board, int(chosenMove)-1):
        if isBoardFull(board):
            winner = "TIE"
            break
        chosenMove = raw_input("{p}, make your move (number 1-9): ".format(
            p = player))
    return chosenMove

def determineWinner(winner):
    if winner == "TIE":
        print "The game ends in a TIE!"
    elif winner == "Player 1":
        print "Player 1 wins!"
    elif winner == "Player 2":
        print "Player 2 wins!"    

while True:
    print "Welcome to Tic Tac Toe!"
    playerToken = ''
    winner = ''
    board = [' ']*9
    
    while not (playerToken == 'X' or playerToken == 'O'):
        playerToken = raw_input("Player 1: Do you want to be X or O? ").upper()
    
        validate = validateUserInput(playerToken) 
        if validate == False:
            print "You must enter X OR O!!!"
    
    players = getPlayerList(playerToken)
    print "Player one has chosen: ", players['p1']
    print "Player two is therefore: ", players['p2']
    printEmptyBoard()
    count = 1
    
    while True:
        #Count to display which player should go next
        playerInput = ' '
        if count % 2 == 0:
            count += 1
            playerInput = getPlayersMove(board, playerInput, "Player 2")
            makeTheMove(board, int(playerInput)-1, players['p2'])
            printTicTacToe(board)
            if didPlayerWin(board, players['p1']):
                winner = "Player 2"
                break
            elif isBoardFull(board):
                winner = "TIE"
                break
            else:
                pass
            
        else:
            count += 1
            playerInput = getPlayersMove(board, playerInput, "Player 1")
            print "The player entered: ", playerInput
            makeTheMove(board, int(playerInput)-1, players['p1'])
            printTicTacToe(board)
            if didPlayerWin(board, players['p1']):
                winner = "Player 1"
                break
            elif isBoardFull(board):
                winner = "TIE"
                break
            else:
                pass
    
    determineWinner(winner)
        
    playAgain = raw_input("Do you wish to play again? (y or n): ")
    if playAgain == 'y' or playAgain == 'Y':
        continue
    else:
        print "Thank you for playing!"
        break